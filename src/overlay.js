livity.css.buildStylesheet([
  '.livity-overlay {\
    display: none;\
    position: fixed;\
    top: 7%;\
    right: 0;\
    bottom: 7%;\
    left: 0;\
  }',
  '.livity-overlay-x {\
    position: fixed;\
    z-index: 1;\
    color: hsla(0, 0%, 75%, 0.65);\
    text-shadow: 0 0 0.2em black;\
    cursor: pointer;\
    top: 12%;\
    right: 5%;\
    font-size: 60px;\
  }',
  '.livity-overlay-x:hover {\
    color: hsla(81, 44%, 75%, 1);\
  }'
])
